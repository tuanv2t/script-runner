Start screen
![start screen.png](https://bitbucket.org/repo/q8nRyd/images/885055551-start%20screen.png)

A start screen will show some sample data. 

Where to input connection string
In the current version, you have to manually input connection string into file ConnectionStrings.xml

Connections are divided into many groups for use, and this is the main purpose of this tool.

Step 1: Select a connection string by selecting Group -> Name

Step 2: Select a FOLDER where contains your scripts

Step 3: Click on button Load script(s) to run, all you script will be loaded and displayed below.
Note: You scripts will shorted by file name. So that, please name your file according to the order it should be run.

All loaded scripts will have status READY.

If you want a script to be ignored,not be run, right click on a script and click Ignored script

![Ingnore Script.png](https://bitbucket.org/repo/q8nRyd/images/2487020178-Ingnore%20Script.png)

The ignored script will have status IGNORED

![Ingnore Script 2.png](https://bitbucket.org/repo/q8nRyd/images/159277045-Ingnore%20Script%202.png)

IGNORED script(s) will not be run.


Step 4:
Click on button Run READY Script(s) to run.
Script will be run from top to down -> order by its name.


If a script has error when running, its status will be changed into ERROR and program stops.

You can exam the error by clicking on the script or double click.

After that you can manually edit script and the click on button "Run READY Script(s)" to run its again.

The program will continue right at the error script.



