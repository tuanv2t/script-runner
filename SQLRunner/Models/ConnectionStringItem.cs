﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SQLRunner.Models
{
    [XmlType("add")]
    public class ConnectionStringItem 
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("connectionString")]
        public string ConnectionString { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
