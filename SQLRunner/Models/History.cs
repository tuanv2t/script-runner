﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRunner.Models
{
    public class History
    {
        public DateTime RunDateTimeUTC { get; set; }
        public string ConnectionGroup { get; set; }
        public string ConnectionName { get; set; }
        public string ConnectionString { get; set; }

    }
}
