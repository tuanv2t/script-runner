﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRunner.Models
{
    public class Script
    {
        public string Path { get; set; }
        public string Status { get; set; }//READY,OK or ERROR
        public string Error { get; set; }
    }
}
