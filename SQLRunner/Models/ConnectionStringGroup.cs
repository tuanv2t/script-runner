﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SQLRunner.Models
{

    [XmlType("ConnectionStringGroup")]
    public class ConnectionStringGroup
    {
        [XmlElement("add")]
        public List<ConnectionStringItem> ConnectionStringItems;
        [XmlAttribute("name")]
        public string Name { get; set; }
    }
}
