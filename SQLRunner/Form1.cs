﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using log4net.Config;
using SQLRunner.Models;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SQLRunner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        private static readonly ILog logger =
        LogManager.GetLogger(typeof(Form1));
        private List<ConnectionStringGroup> _connectionStringGroupList;
        private List<string> _alreadyRunConnectionStringList = new List<string>();
        private void Form1_Load(object sender, EventArgs e)
        {

            DOMConfigurator.Configure();

            _connectionStringGroupList = LoadConnectionStringXml();
            //Show the connection list on form for user to select
            var connectionGroups = _connectionStringGroupList.Select(x => x.Name).OrderBy(x => x).ToList();
            cmbConnectionGroup.DataSource = connectionGroups;

            //load the Sample data to test :)
            txtFolder.Text = string.Format(@"{0}Sample data", AppDomain.CurrentDomain.BaseDirectory);
            LoadFileOnList();
        }

        private List<ConnectionStringGroup> LoadConnectionStringXml()
        {
            //XmlSerializer deserializer = new XmlSerializer(typeof(List<ConnectionStringGroup>));
            //TextReader textReader = new StreamReader(@"ConnectionStrings.xml");
            //List<ConnectionStringGroup> conns;
            //conns = (List<ConnectionStringGroup>)deserializer.Deserialize(textReader);
            //textReader.Close();
            
            TextReader textReader = new StreamReader(@"ConnectionStrings.xml");
            var content = textReader.ReadToEnd();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "ConnectionStringSet";
            // xRoot.Namespace = "http://www.cpandl.com";
            xRoot.IsNullable = true;
            //parse data xml sent from client
            var deSerializer = new XmlSerializer(typeof(List<ConnectionStringGroup>), xRoot);
            bool isOK = true;
            var result = new List<ConnectionStringGroup>();
            try
            {
                using (TextReader reader = new StringReader(content))
                {
                    result = (List<ConnectionStringGroup>) deSerializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                isOK = false;
            }

            return result;
        }

        private void cmbConnectionGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get selected connection group
            if (cmbConnectionGroup.SelectedIndex >= 0)
            {
                var item = cmbConnectionGroup.Items[cmbConnectionGroup.SelectedIndex].ToString();
                cmbConnectionString.DataSource = null;
                cmbConnectionString.Items.Clear();
                cmbConnectionString.DataSource =
                    _connectionStringGroupList.FirstOrDefault(x => x.Name.Equals(item))
                        .ConnectionStringItems.OrderBy(x => x.Name).ToList();
            }
            
        }

        private void cmbConnectionString_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbConnectionString.SelectedIndex >= 0)
            {
                var item = (ConnectionStringItem) cmbConnectionString.Items[cmbConnectionString.SelectedIndex];
                txtConnectionString.Text = item.ConnectionString;
            }

        }

        private void btnLoadScript_Click(object sender, EventArgs e)
        {
            //load scripts to run
            if (string.IsNullOrEmpty(txtFolder.Text))
            {
                MessageBox.Show("Please select a folder contains script(s)");
                return;
            }
            //check if the directory in textbox is valid of not
            if (!Directory.Exists(txtFolder.Text.Trim()))
            {
                MessageBox.Show("Selected folder does not exist");
                return;
            }
            LoadFileOnList();
           
        }

        private void LoadFileOnList()
        {
            var fileHelper = new FileHelper();
            var files = fileHelper.LoadFileFromFolder(txtFolder.Text.Trim(), "*.sql");
            listView1.Items.Clear();
            //display to listview
            foreach (var file in files)
            {
                var script = new Script() { Path = file, Status = "READY" };
                var item = new ListViewItem(new string[] { file, "READY" });
                item.Tag = script;
                listView1.Items.Add(item);
            }
        }
        private void btnRun_Click(object sender, EventArgs e)
        {
            // run script
            //get the selected connection string first
            var connGroup = cmbConnectionGroup.Items[cmbConnectionGroup.SelectedIndex].ToString();
            var conn = cmbConnectionString.Items[cmbConnectionString.SelectedIndex].ToString();
            var connectionStringDetail = txtConnectionString.Text;
            var message = string.Format("Do you want to run script(s) on database [{0}]->[{1}]:\n{2}", connGroup, conn,
                connectionStringDetail);
            if (MessageBox.Show(message, "Confirm running script(s)", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
            {
                return;
            }
            //if (_alreadyRunConnectionStringList.Contains(connectionStringDetail))
            //{
            //    MessageBox.Show("This database server has been already run. It will not be run. If there's any error, please manually correct the script and it's better to run that script directly on database.");
            //    return;
            //}
            var start = DateTime.UtcNow;
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                var script = (Script)((ListViewItem)(listView1.Items[i])).Tag;
                if (listView1.Items[i].SubItems[1].Text == "READY"|| listView1.Items[i].SubItems[1].Text == "ERROR")//only run the script that has not been run or ERROR
                {
                    try
                    {
                        logger.Info(string.Format("Start running:\t {0}\t at: {1}", script.Path, DateTime.UtcNow));
                        using (StreamReader sr = File.OpenText(script.Path))
                        {
                            string content = sr.ReadToEnd();
                            SqlConnection sqlConn = new SqlConnection(connectionStringDetail);

                            Server server = new Server(new ServerConnection(sqlConn));

                            server.ConnectionContext.ExecuteNonQuery(content);

                            listView1.Items[i].SubItems[1].Text = "OK";
                            listView1.Items[i].ForeColor = Color.Green;
                            logger.Info(string.Format("Stop with success:\t {0}\t at: {1}", script.Path, DateTime.UtcNow));
                        }
                    }
                    catch (Exception ex)
                    {
                        listView1.Items[i].SubItems[1].Text = "ERROR";
                        listView1.Items[i].ForeColor = Color.Red;
                        script.Error = GetFullException(ex);
                        logger.Info(string.Format("Stop with error:\t {0}\t at: {1} \n.Error: {2}", script.Path, DateTime.UtcNow, script.Error));
                        
                        MessageBox.Show("ERROR when running " + script.Path + "\n " + script.Error);
                        break;//stop 
                    }
                }
            }

            _alreadyRunConnectionStringList.Add(connectionStringDetail);//remember the connectionString has been run for this session
            var history = new History()
            {
                RunDateTimeUTC = start,
                ConnectionGroup = connGroup,
                ConnectionName = conn,
                ConnectionString = connectionStringDetail
            };
            var item = new ListViewItem(new string[] { start.ToString("yyyy-MM-dd H:mm:ss zzz"), connGroup, conn, connectionStringDetail });
            item.Tag = history;
            listViewHistory.Items.Add(item);
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems != null && listView1.SelectedItems.Count > 0)
            {
                //get the first selected script
                var item = (ListViewItem) listView1.SelectedItems[0];
                var script = (Script) item.Tag;
                using (StreamReader sr = File.OpenText(script.Path))
                {
                    string content = sr.ReadToEnd();
                    txtScriptContent.Text = content;
                }
                if (item.SubItems[1].Text == "ERROR")
                {
                    //show the error message
                    MessageBox.Show(script.Error);
                }
            }
        }

        private string GetFullException(Exception ex)
        {
            string exception = ex.Message;
            if (ex.InnerException != null)
            {
                exception += "\n " + GetFullException(ex.InnerException);
            }
            return exception;
        }

        private void listViewHistory_DoubleClick(object sender, EventArgs e)
        {
            if (listViewHistory.SelectedItems != null && listViewHistory.SelectedItems.Count > 0)
            {
                //get the first selected script
                var item = (ListViewItem)listViewHistory.SelectedItems[0];
                var history = (History)item.Tag;
                var message = string.Format("Time:\t\t{0}\nConnection Group:\t{1}-\n Connection Name:\t{2}\n Connection Detail:\t{3}",
                    history.RunDateTimeUTC.ToString("yyyy-MM-dd H:mm:ss zzz"),
                    history.ConnectionGroup, history.ConnectionName, history.ConnectionString);
                MessageBox.Show(message);
            }
        }

        private void btnTestConn_Click(object sender, EventArgs e)
        {
            if (txtConnectionString.Text.Trim().Length > 0)
            {
                using (SqlConnection sqlConn = new SqlConnection(txtConnectionString.Text.Trim()))
                {
                    try
                    {
                        sqlConn.Open();
                        MessageBox.Show("The connection is OK");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to connect to database");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }

            }
            else
            {
                MessageBox.Show("Please select connection string");
            }
        }

        private void contextMenuStripScript_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void contextMenuStripScript_Click(object sender, EventArgs e)
        {
            var clickedMenuItem = sender as ContextMenuStrip;
            if (clickedMenuItem != null)
            {
                var menuText = clickedMenuItem.Text;

                switch (menuText)
                {
                    case "Ignore script":
                        //MessageBox.Show("Ignore Script");
                        IgnoreSelectedScript();
                        break;

                    case "Lion":
                        break;
                }
            }
           
        }

        private void IgnoreSelectedScript()
        {
            if (listView1.SelectedItems != null && listView1.SelectedItems.Count > 0)
            {
                //get the first selected script
                var item = (ListViewItem) listView1.SelectedItems[0];
                var script = (Script) item.Tag;
                item.SubItems[1].Text = "IGNORED";
                item.BackColor = Color.Yellow;
                logger.Info(string.Format("Ignore:\t {0}\t at: {1}", script.Path, DateTime.UtcNow));
            }
        }
    }
}
