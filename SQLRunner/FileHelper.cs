﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLRunner
{
    public class FileHelper
    {
        public List<string> LoadFileFromFolder(string path, string searchPattern)
        {
            string[] filePaths = Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories);
            var result =  filePaths.OrderBy(x => x).ToList();
            return result;
        }
    }
}
